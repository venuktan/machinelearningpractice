__author__ = 'venuktangirala'

import numpy as np
from numpy import loadtxt, where, exp

import scipy.optimize as opt

def sigmoid(z):

    denominator = 1.0 + exp(-1*z)

    return (1.0/denominator)

def computeCost(X,y,theta):

    p_1 = sigmoid(np.dot(X,theta))

    m = y.size

    J = np.sum( -y * np.log(p_1) - ( (1-y) * np.log(1 - p_1) ) )

    J = J/m

    return J

def gradientDescent(X,y,theta):

    p_1 = sigmoid(np.dot(X,theta))

    error = p_1 - y

    g = np.dot(error,X)

    return g.mean()


data = loadtxt('ex2data1.txt',delimiter=',')
X = data[:,0:2]
y = data[:,2]
# theta = [0,0]
theta = 0.1* np.random.randn(2)
X_1 = np.append( np.ones((X.shape[0], 1)), X, axis=1)

cost= computeCost(X,y,theta)
grad = gradientDescent(X,y,theta)

print "theta="+ str(theta)
print 'cost='+ str(cost)
print 'grad='+str(grad)

theta_1 = opt.fmin_bfgs(cost, theta, fprime=grad, args=(X, y))


