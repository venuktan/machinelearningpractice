__author__ = 'venuktangirala'
from bokeh.plotting import scatter
from pylab import scatter ,show,legend,xlabel,ylabel
from numpy import loadtxt, where, exp

def plotData(X,y):


    pos = where(y==1)
    neg = where(y==0)

    scatter(X[pos, 0], X[pos, 1], marker='o', c='b')
    scatter(X[neg, 0], X[neg, 1], marker='x', c='r')
    xlabel('Exam 1 score')

    ylabel('Exam 1 score')

    legend(['Not Admitted','Admitted'])

    show()

def ComputeCost(X,y,theta):
    m = y.size

    print X
    print theta

    predictions = X.dot(theta).flatten()

    print predictions

    sqErrors =  (predictions - y ) ** 2

    J = (1.0/(2 * m)) * sqErrors.sum()

    return J

data = loadtxt('ex2data1.txt',delimiter=',')
X = data[:,0:2]
y = data[:,2]
# theta = [0,0]